﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dojodachi.Models;
using Microsoft.AspNetCore.Http;

namespace Dojodachi.Controllers
{
    
    public class HomeController : Controller
    {
        [HttpGet("")]
        public IActionResult Index()
        {
            DojodachiPet thisDojodachi = HttpContext.Session.GetObjectFromJson<DojodachiPet>("thisDojodachi");

            if(thisDojodachi == null)
            {
                DojodachiPet newDojodachi = new DojodachiPet();

                HttpContext.Session.SetObjectAsJson("thisDojodachi", newDojodachi);
                return View(newDojodachi);
            }

            return View(thisDojodachi);
        }

        [HttpPost("feed")]
        public IActionResult Feed()
        {
            DojodachiPet thisDojodachi = HttpContext.Session.GetObjectFromJson<DojodachiPet>("thisDojodachi");
            bool isSuccessful = SuccessDeterminator();

            if(thisDojodachi.Meals < 1)
            {
                ResultObject thisResult = new ResultObject("sad.png", "Oh no! You don't have enough meals to feed your Dojodachi. You have to go to work to gain more meals.","fullness",thisDojodachi.Fullness);

                return Json(thisResult);
            }

            else
            {
                if (!isSuccessful)
                {
                    thisDojodachi.Meals--;
                    ResultObject thisResult = new ResultObject("upset.png", "You tried to feed your Dojodachi, but for some reason they didn't like it. Better luck next time.", "fullness", thisDojodachi.Fullness, "meals", thisDojodachi.Meals);

                    HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);

                    return Json(thisResult);
                }

                Random rand = new Random();
                var consumeValue = rand.Next(5,11);
                thisDojodachi.Fullness += consumeValue;
                thisDojodachi.Meals--;

                HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);

                return Json(new ResultObject("content.png", "You fed your Dojodachi and they gained " + consumeValue + " fullness!", "fullness", thisDojodachi.Fullness, "meals", thisDojodachi.Meals));
            }
        }

        [HttpPost("play")]
        public IActionResult Play()
        {
            DojodachiPet thisDojodachi = HttpContext.Session.GetObjectFromJson<DojodachiPet>("thisDojodachi");
            bool isSuccessful = SuccessDeterminator();

            if(thisDojodachi.Energy < 5)
            {
                ResultObject thisResult = new ResultObject("irritated.png", "Your Dojodachi is cranky and doesn't want to play. They could really use a nap!","energy",thisDojodachi.Energy);

                return Json(thisResult);
            }

            else
            {
                if (!isSuccessful)
                {
                    thisDojodachi.Energy -= 5;
                    ResultObject thisResult = new ResultObject("angry.png", "Yikes! Your Dojodachi did not like playing. Maybe another time.", "energy", thisDojodachi.Energy);

                    HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);
                    
                    return Json(thisResult);
                }

                Random rand = new Random();
                var happyValue = rand.Next(5,11);
                thisDojodachi.Happiness += happyValue;
                thisDojodachi.Energy -= 5;

                HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);

                ResultObject finalResult = new ResultObject("happy.png", "How fun! You played with your Dojodachi, and they gained " + happyValue + " happiness!", "happiness", thisDojodachi.Happiness, "energy", thisDojodachi.Energy);
                
                return Json(finalResult);
            }
        }

        [HttpPost("work")]
        public IActionResult Work()
        {
            DojodachiPet thisDojodachi = HttpContext.Session.GetObjectFromJson<DojodachiPet>("thisDojodachi");

            if(thisDojodachi.Energy < 5)
            {
                ResultObject thisResult = new ResultObject("irritated.png", "Your Dojodachi is tired and doesn't want to go to work. They could really use a nap!","energy",thisDojodachi.Energy);

                return Json(thisResult);
            }

            else
            {
                Random rand = new Random();
                var mealValue = rand.Next(1,4);
                thisDojodachi.Meals += mealValue;
                thisDojodachi.Energy -= 5;

                ResultObject finalResult = new ResultObject("thinking.png", "All that hard work paid off! You gain " + mealValue + " meals!", "meals", thisDojodachi.Meals, "energy", thisDojodachi.Energy);
                
                HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);

                return Json(finalResult);
            }
        }

        [HttpPost("sleep")]
        public IActionResult Sleep()
        {
            DojodachiPet thisDojodachi = HttpContext.Session.GetObjectFromJson<DojodachiPet>("thisDojodachi");

            thisDojodachi.Energy += 15;
            thisDojodachi.Fullness -= 5;
            thisDojodachi.Happiness -= 5;

            ResultObject thisResult = new ResultObject("sleep.png", "Your Dojodachi rests and gains 15 energy back.", "energy", thisDojodachi.Energy, "happiness", thisDojodachi.Happiness, "fullness", thisDojodachi.Fullness);

            HttpContext.Session.SetObjectAsJson("thisDojodachi", thisDojodachi);
            
            return Json(thisResult);
        }

        [HttpPost("restart")]
        public IActionResult Restart()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        public bool SuccessDeterminator()
        {
            Random rand = new Random();
            int result = rand.Next(1,5);
            if(result == 1)
            {
                return false;
            }
            return true;
        }

    }
}

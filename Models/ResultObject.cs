using System;

namespace Dojodachi.Models
{
    public class ResultObject
    {
       public string imgString { get; }
       public string result { get; }
       public string statKey { get; }
       public int statValue { get; }
       public string secondaryKey { get; }
       public int secondaryValue { get; }
       public string thirdKey { get; }
       public int thirdValue { get; }

       public ResultObject(string imgstr, string res, string statk, int statv)
       {
           imgString = imgstr;
           result = res;
           statKey = statk;
           statValue = statv;
           secondaryKey = null;
           secondaryValue = 0;
           thirdKey = null;
           thirdValue = 0;
       }

       public ResultObject(string imgstr, string res, string statk, int statv, string secondkey, int secondvalue)
       {
           imgString = imgstr;
           result = res;
           statKey = statk;
           statValue = statv;
           secondaryKey = secondkey;
           secondaryValue = secondvalue;
           thirdKey = null;
           thirdValue = 0;
       }

       public ResultObject(string imgstr, string res, string statk, int statv, string secondkey, int secondvalue, string thirdk, int thirdv)
       {
           imgString = imgstr;
           result = res;
           statKey = statk;
           statValue = statv;
           secondaryKey = secondkey;
           secondaryValue = secondvalue;
           thirdKey = thirdk;
           thirdValue = thirdv;
       }

    }
}
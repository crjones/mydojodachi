﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$("#playAgain").toggle(false);
$("li form").submit(function(e){
    e.preventDefault();
    $.ajax({
        method: 'POST',
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function(response){
            console.log(response);
            $("#dojodachi-box img").attr("src", "images/" + response.imgString);
            $("#dojodachi-box p").html(response.result);
            $("#" + response.statKey).html(response.statValue);
            $("#" + response.secondaryKey).html(response.secondaryValue);
            $("#" + response.thirdKey).html(response.thirdValue);

            if ($("#happiness").html() > 99 && $("#energy").html() > 99 && $("#fullness").html() > 99)
            {
                $("#gameOverModal").modal('show');
                $(".modal-title").html("You Won!");
                $("#gameOverModal p").html("You've done it! You are a Dojodachi master and the owner of a very happy Dojodachi!");
                $("#gameOverModal img").attr("src", "images/happy.png");
                $("ul form input").toggle(false);
                $("#playAgain").toggle(true);
            }
            if ($("#happiness").html() <= 0)
            {
                $("#gameOverModal").modal('show');
                $(".modal-title").html("You Lose!");
                $("#gameOverModal p").html("Oh no! Your Dojodachi has reached 0 happiness. I don't think they'll ever be happy again! Poor fella...");
                $("#gameOverModal img").attr("src", "images/unhappy.png");
                $("ul form input").toggle(false);
                $("#playAgain").toggle(true);
            }
            if ($("#fullness").html() <= 0)
            {
                $("#gameOverModal").modal('show');
                $(".modal-title").html("You Lose!");
                $("#gameOverModal p").html("Your Dojodachi has reached 0 fullness and has died of starvation. It's a good thing this is just a game, right?");
                $("#gameOverModal img").attr("src", "images/starving.png");
                $("ul form input").toggle(false);
                $("#playAgain").toggle(true);
            }
        }
    })
})